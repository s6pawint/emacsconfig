# EmacsConfig

This are my configuration files for GNU Emacs. It will use elpaca as a package manager and several packages will be installed. This will also set up vim-keybindings and custom keybindings.